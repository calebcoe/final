﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    public static PlayerController instance;
    //add a slot for the Rigidbody
    public Rigidbody2D theRB;
    //add slot for player move speed
    public float moveSpeed = 5f;
    
    //adds controls for the player movement
    private Vector2 moveInput;
    //adds controls for the mouse to be moved for view
    private Vector2 mouseInput;
    //adds slot for mouse sensitivity
    public float mouseSensitivity = 3f;
    //adds slot for camera view
    public Camera viewCam;
    //adds a game object for the bullet impact 
    public GameObject bulletImpact;
    //adds slot for current ammo... this will adjust later when ammo packs are picked up
    public int currentAmmo;
    //adds slot for gun animation
    public Animator gunAnim;
    //adds slot for other animation
    public Animator anim;
    //adds slot for current health that will adjust based on damage taken from enemies and when health packs are picked up 
    public int currentHealth;
    //adds slot for max health
    public int maxHealth = 100;
    //adds game object for main menu
    public GameObject mainMenu;
    //adds game object for death screen
    public GameObject deadScreen;
    //adds a boolean that can be true or false based on the players health
    private bool hasDied;
    //adds the ability to have the ammo and health text change based on game interactions
    public Text healthText, ammoText;

    //runs the player script
    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        //sets current health to max health when the game starts
        currentHealth = maxHealth;
        //Set the health text in the UI to be equal to the value of the currentHealth converted to a string
        healthText.text = currentHealth.ToString() + "%";
        //Set the ammo text in the UI to be equal to the value of the currentAmmo converted to a string
        ammoText.text = currentAmmo.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasDied)
        {
            //KEYBOARD MOVEMENT
            //Set moveInput to a new Vector2 that retrieves the new x and y
            moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            //Move left and right
            Vector3 moveHorizontal = transform.up * -moveInput.x;

            //Move forwards and backwards
            Vector3 moveVertical = transform.right * moveInput.y;

            //Apply movement to the rigidbody
            theRB.velocity = (moveHorizontal + moveVertical) * moveSpeed;


            //MOUSE MOVEMENT

            mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")) * mouseSensitivity;

            //Allows flat camera movement (no up and down)
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z - mouseInput.x);

            //localRotation will select just the local xyz instead of adding child elements 
            //The new Vector3 will only change the y-axis and not the x or z
            viewCam.transform.localRotation = Quaternion.Euler(viewCam.transform.localRotation.eulerAngles + new Vector3(0f, mouseInput.y, 0f));

            //Shooting
            
            if (Input.GetMouseButtonDown(0))
            {
                // if the player has ammo, it will do stuff
                if (currentAmmo > 0)
                {
                    Ray ray = viewCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        Debug.Log("I'm looking at " + hit.transform.name);
                        Instantiate(bulletImpact, hit.point, transform.rotation);

                        if (hit.transform.tag == "Boss")
                        {
                            //States that the Boss is taking damage from the player
                            hit.transform.parent.GetComponent<BossController>().TakeDamage();
                        }
                    }
                    else
                    {
                        Debug.Log("I'm looking at nothing.");
                    }

                    // if the player has ammo, it will do stuff
                    if (Physics.Raycast(ray, out hit))
                    {
                        Debug.Log("I'm looking at " + hit.transform.name);
                        Instantiate(bulletImpact, hit.point, transform.rotation);

                        if (hit.transform.tag == "Enemy")
                        {
                            //States that the Enemy that shoots will take damage from the player
                            hit.transform.parent.GetComponent<EnemyController>().TakeDamage();
                        }
                    }
                    else
                    {
                        Debug.Log("I'm looking at nothing.");
                    }

                    // if the player has ammo, it will do stuff
                    if (Physics.Raycast(ray, out hit))
                    {
                        Debug.Log("I'm looking at " + hit.transform.name);
                        Instantiate(bulletImpact, hit.point, transform.rotation);

                        if (hit.transform.tag == "Enemy")
                        {
                            //States that the Enemy that uses melee will take damage from the player
                            hit.transform.parent.GetComponent<EnemyMelee>().TakeDamage();
                        }
                    }
                    else
                    {
                        Debug.Log("I'm looking at nothing.");
                    }

                    currentAmmo--;
                    //Sets trigger saved in shooting animation
                    gunAnim.SetTrigger("Shoot");
                    UpdateAmmoUI();
                }
            }

            //tells the engine if the player is moving or not
            if (moveInput != Vector2.zero)
            {
                anim.SetBool("isMoving", true);
            } else
            {
                anim.SetBool("isMoving", false);
            }
        }
    }

    //Damage is taken from enemies that shoot
    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;

        if(currentHealth <= 0)
        {
            deadScreen.SetActive(true);
            hasDied = true;
            currentHealth = 0;
        }

        healthText.text = currentHealth.ToString() + "%";
    }

    //Damage is taken from enemies that use melee
    public void TakeMeleeDamage(int meleeDamageAmount)
    {
        currentHealth -= meleeDamageAmount;

        if (currentHealth <= 0)
        {
            deadScreen.SetActive(true);
            hasDied = true;
            currentHealth = 0;
        }

        healthText.text = currentHealth.ToString() + "%";
    }

    //Damage is taken by the boss
    public void TakeBossDamage(int bossMeleeDamageAmount)
    {
        currentHealth -= bossMeleeDamageAmount;

        if (currentHealth <= 0)
        {
            deadScreen.SetActive(true);
            hasDied = true;
            currentHealth = 0;
        }

        healthText.text = currentHealth.ToString() + "%";
    }

    //adds health when a med pack is picked up
    public void AddHealth(int healAmount)
    {
        //health will be added 
        currentHealth += healAmount;
        //makes sure that health will never be over max
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        
        //health will display on screen
        healthText.text = currentHealth.ToString() + "%";  
    }


    //ammo will updated and display on screen 
    public void UpdateAmmoUI()
    {
        ammoText.text = currentAmmo.ToString();
    }


    //adds slot for start of game
    public void StartGame()
    {

    }
}
