﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //If the player presses escape unlock the cursor

        if (Input.GetKeyDown(KeyCode.Escape))
        {
           UnlockCursor();
        }
        
        

        //If the player clicks lock the cursor again

        if (Input.GetMouseButtonUp(0))
        {
            LockCursor();
        }
        
        
    }

    //Set a function for LockCursor
    private void LockCursor()
    {
        //Locks the cursor into position so nothing is accidentally clicked
        Cursor.lockState = CursorLockMode.Locked;

        //Makes the cursor invisible
        Cursor.visible = false;
    }

    private void UnlockCursor()
    {
        //Unlocks the cursor
        Cursor.lockState = CursorLockMode.None;
        //Makes the cursor visible
        Cursor.visible = true;
    }

    private void StartGame()
    {

    }
}
